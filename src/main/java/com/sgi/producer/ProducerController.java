package com.sgi.producer;

import com.sgi.producer.common.Order;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProducerController {
    private final Logger logger = LoggerFactory.getLogger(ProducerApplication.class);

    @Autowired
    private KafkaTemplate<Object, Object> template;

    @PostMapping(path = "/api/order")
    public void sendFoo(@RequestBody Order newOrder) {
        logger.debug("New order sent: " + newOrder.toString());
        this.template.send("order-topic", newOrder);
    }
}
